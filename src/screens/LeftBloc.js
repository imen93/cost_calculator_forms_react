import React, { Component } from 'react';
import OBCard from '../components/OBCard';
import { Container } from 'reactstrap';
import "../css/LeftBloc.css"
import OBLine from '../components/OBLine';



class LeftBloc extends Component {


    _renderListCard = () => {
        return (
            <Container>
                <OBCard
                    source=""
                    title="Set costs"
                    description="Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after products." />

                <OBCard
                    source=""
                    title="Calculate"
                    description="Energistically scale future-proof core competencies vis-a-vis impactful experiences. Dramatically synthesize integrated schemas." />


                <OBCard
                    source=""
                    title="Contact us"
                    description="Quickly deploy strategic networks with compelling e-business. Credibly pontificate hig. and enabled data." />

            </Container>
        );
    }

    render() {
        return (
            <Container style={{ width: "25vw", height: "100%", backgroundColor: "white", marginTop: 50 }}>
                <h1 className="mytitle">Calculate your</h1>
                <h1 className="mySubTitle">Project Costs</h1>
                <p className="description">Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing.</p>
                <OBLine backgroundColor="gray" height="0.1em" />
                {this._renderListCard()}
            </Container>

        );
    }
}
export default LeftBloc;