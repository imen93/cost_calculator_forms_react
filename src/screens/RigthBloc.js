import React, { Component } from 'react';
import { Container, Col, Row } from 'reactstrap';
import Calendar from 'react-calendar';
import OBLine from '../components/OBLine';
import ToggleSwitch from '../components/ToggleSwitch';
import OBInputText from '../components/OBInputText';

import 'react-dropdown/style.css';
import 'react-day-picker/lib/style.css';
import "../css/RigthBloc.css"
import '../css/DropDownMenu.css'
import '../css/InputRange.css'
import '../css/Calendar.css';
const datas_project_type = ["Software developement", "Webbsite production", "Online application", "Mobile application", "Ios application", "Windows application", "Product design", "Product improvment"]
const datas_project_speed = ["Regular", "Fast", "I need it urgently"]
const datas_project_precision = ["Regular", "Very pricise", "Pixel perfect"]
const datas_team_dedication = ["Regular", "Dedicated", "They dont have personal life anyway"]
const datas_project_start_time = ["08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00"]



class RigthBloc extends Component {

    constructor() {
        super()
        this.state = {
            showNext: false,
            displayMenuProjectType: false,
            displayMenuProjectSpeed: false,
            displayMenuProjectPrecision: false,
            displayMenuTeamDedicated: false,
            displayMenuProjectStartTime: false,
            project_type: "",
            project_speed: "",
            project_precision: "",
            project_start_time: "",
            team_dedicated: "",
            input_name: "",
            input_email: "",
            input_adresse: "",
            input_phone: "",
            showError: false,
            styleInputName: null,
            styleInputEmail: null,
            x_day: 0,
            x_team: 0,
            left_x_team: 0,
            left_x_day: 0,
            date: "",
            dateInput: "",
            isShowDate: false,
        }
    }



    //fonction qui permet de gérer le state displayMenuProjectType
    showDropdownMenuProjectType = (event) => {
        event.preventDefault();
        this.setState({ displayMenuProjectType: true }, () => {
            document.addEventListener('click', this.hideDropdownMenuProjectType);
        });
    }

    hideDropdownMenuProjectType = () => {
        this.setState({ displayMenuProjectType: false }, () => {
            document.removeEventListener('click', this.hideDropdownMenuProjectType);
        });

    }

    //fonction qui permet de gérer le state displayMenuProjectSpeed
    showDropdownMenuProjectSpeed = (event) => {
        event.preventDefault();
        this.setState({ displayMenuProjectSpeed: true }, () => {
            document.addEventListener('click', this.hideDropdownMenuProjectSpeed);
        });
    }

    hideDropdownMenuProjectSpeed = () => {
        this.setState({ displayMenuProjectSpeed: false }, () => {
            document.removeEventListener('click', this.hideDropdownMenuProjectSpeed);
        });

    }

    //fonction qui permet de gérer le state displayMenuProjectPrecision
    showDropdownMenuProjectPrecision = (event) => {
        event.preventDefault();
        this.setState({ displayMenuProjectPrecision: true }, () => {
            document.addEventListener('click', this.hideDropdownMenuProjectPrecision);
        });
    }

    hideDropdownMenuProjectPrecision = () => {
        this.setState({ displayMenuProjectPrecision: false }, () => {
            document.removeEventListener('click', this.hideDropdownMenuProjectPrecision);
        });

    }

    //fonction qui permet de gérer le state displayMenuTeamDedicated
    showDropdownMenuTeamDedicated = (event) => {
        event.preventDefault();
        this.setState({ displayMenuTeamDedicated: true }, () => {
            document.addEventListener('click', this.hideDropdownMenuTeamDedicated);
        });
    }
    hideDropdownMenuTeamDedicated = () => {
        this.setState({ displayMenuTeamDedicated: false }, () => {
            document.removeEventListener('click', this.hideDropdownMenuTeamDedicated);
        });

    }

    //fonction qui permet de gérer le state displayMenuProjectStartTime
    showDropdownMenuProjectStartTime = (event) => {
        event.preventDefault();
        this.setState({ displayMenuProjectStartTime: true }, () => {
            document.addEventListener('click', this.hideDropdownMenuProjectStartTime);
        });
    }
    hideDropdownMenuProjectStartTime = () => {
        this.setState({ displayMenuProjectStartTime: false }, () => {
            document.removeEventListener('click', this.hideDropdownMenuProjectStartTime);
        });

    }

    onClickHandlerProjectType = (item) => {
        this.setState({
            project_type: item
        })

    }

    onClickHandlerProjectSpeed = (item) => {
        this.setState({
            project_speed: item
        })

    }

    onClickHandlerProjectPrecision = (item) => {
        this.setState({
            project_precision: item
        })

    }

    onClickHandlerTeamDedicated = (item) => {
        this.setState({
            team_dedicated: item
        })

    }

    onClickHandlerProjectStartTime = (item) => {
        this.setState({
            project_start_time: item
        })

    }

    //fonction qui permet de gérer l'actions sur le boutton next
    onClickNext = () => {
        this.setState({
            showNext: true,
        })
    }

    //fonction qui permet de gérer l'actions sur le boutton sumit
    //permet de verifier si les champs(email et nom) ne sont pas vide
    onClickSubmit = () => {
        let styleError = { borderWidth: 2, borderStyle: "Solid", borderColor: "#fb17ce", boxShadow: 3 }
        let styleValid = { borderWidth: 2, borderStyle: "Solid", borderColor: "#c8c8c8", boxShadow: 3 }

        if (this.state.input_name === "" && this.state.input_email === "") {
            this.setState({ styleInputName: styleError, styleInputEmail: styleError, showError: true })
            return true
        } else if (this.state.input_name === "") {
            this.setState({ styleInputName: styleError, styleInputEmail: styleValid, showError: true })
            return true
        }
        else if (this.state.input_email === "") {
            this.setState({ styleInputEmail: styleError, styleInputName: styleValid, showError: true })
            return true
        }
        else {
            this.setState({ styleInputName: styleValid, styleInputEmail: styleValid, showError: false })
            return false
        }

    }


    //fonction qui permet de mettre à jour le state input_name
    handleChangeName = (event) => {
        this.setState({ input_name: event.target.value });
    }
    //fonction qui permet de mettre à jour le state input_nemail
    handleChangeEmail = (event) => {
        this.setState({ input_email: event.target.value });
    }
    //fonction qui permet de mettre à jour le state input_phone
    handleChangePhone = (event) => {
        this.setState({ input_phone: event.target.value });
    }
    //fonction qui permet de mettre à jour le state input_adresse
    handleChangeAdresse = (event) => {
        this.setState({ input_adresse: event.target.value });
    }
    // 
    handleChangeDay = (event) => {
        this.setState({ x_day: event.target.value });
    }
    handleChangeTeam = (event) => {
        this.setState({ x_team: event.target.value });
    }
    // fonction qui permet de mettre à jour le state date
    onChange = (date) => {
        var date1 = new Date(date),
            mnth = ("0" + (date1.getMonth() + 1)).slice(-2),
            day = ("0" + date1.getDate()).slice(-2);
        let result = [day, mnth, date1.getFullYear(),].join("/");
        this.setState({ dateInput: result, date: date, isShowDate: false })

    }
    //fonction qui permet d'afficher le calendrier
    schowDate = (event) => {
        this.setState({ isShowDate: true })
    }

    //fonction qui permet de rendre le partie 2 de la pages aprés le boutton next
    _renderNext = () => {
        return (
            <div>
                <Row>
                    <Col md="6"> <OBInputText type="text" placeholder="Name" value={this.state.input_name} handleChange={this.handleChangeName} style={this.state.styleInputName} /></Col>
                    <Col md="6"> <OBInputText type="text" placeholder="Email" value={this.state.input_email} handleChange={this.handleChangeEmail} style={this.state.styleInputEmail} /></Col>
                </Row>
                <Row>
                    <Col md="6"> <OBInputText type="text" placeholder="Phone" value={this.state.input_phone} handleChange={this.handleChangePhone} /></Col>
                    <Col md="6">  <OBInputText type="text" placeholder="Adresse" value={this.state.input_adresse} handleChange={this.handleChangeAdresse} /></Col>
                </Row>
                <Row>
                    <Col md="6">
                        <div onClick={this.schowDate}>
                            <OBInputText type="text" placeholder="Prefered projet start date" value={this.state.dateInput} />
                            {this.state.isShowDate ?
                                <Calendar
                                    onChange={(date) => this.onChange(date)}
                                    value={this.state.date}
                                    nextLabel="Next"
                                    next2Label=""
                                    prevLabel="Prev"
                                    prev2Label=""
                                /> : null}
                        </div>
                    </Col>
                    <Col md="6" style={{ marginTop: 16 }}>
                        <div onClick={this.showDropdownMenuProjectStartTime}>
                            <input className="inputzone" placeholder="Preferred project start time" type="text" value={this.state.project_start_time} />
                            <img className="arrow" src={this.state.displayMenuProjectStartTime ? require("../images/up-arrow.png") : require("../images/down-arrow.png")} />
                        </div>
                        {this.state.displayMenuProjectStartTime ? (
                            <div className="menuList">
                                <h1 className="h1-select" onClick={() => this.onClickHandlerProjectStartTime("")}>Select...</h1>
                                {
                                    datas_project_start_time.map((item) =>
                                        (this.state.project_start_time === item) ?
                                            <li style={{ listStyleType: "disc", color: "#fb17ce" }}><span>{item}</span></li>
                                            :
                                            <li onClick={() => this.onClickHandlerProjectStartTime(item)}><span>{item}</span></li>
                                    )
                                }
                            </div>
                        ) :
                            (
                                null
                            )
                        }
                    </Col>



                </Row>
                <Row>
                    <Col md="12"> <textarea placeholder="Message" className="textareaInput" /></Col>
                </Row>
                <Row>
                    <div class="innerCheckbox checkboxInput">
                        <input type="checkbox" value="1" id="checkboxInput1" />
                        <label class="inner" for="checkboxInput1"></label>
                        <label class="outer" for="checkboxInput1">Email me quote!</label>
                    </div>

                </Row>
                <Row>
                    <Col md="5"></Col>
                    <Col md="2">
                        <button className='bottomNext' type="submit" onClick={this.onClickSubmit}>
                            <span className="textNext"> Submit</span>
                            <img className='iconNext' src={require("../images/next.png")} />
                        </button>
                    </Col>
                </Row>
                {
                    this.state.showError ?
                        <Row>
                            <Col md="4"></Col>
                            <Col>
                                <h1 className="h1-labbelError">Please fill out all required fields.</h1>
                            </Col>
                        </Row>
                        : null

                }

            </div>
        )
    }

    render() {
        return (
            <div style={{ width: "60vw", height: "100%", backgroundColor: "#f5F5F5", paddingBottom: 16 }}>
                <Container style={{ marginTop: 50 }}>

                    <Row>
                        <Col className="h1-title">
                            Define project type
                    </Col>
                    </Row>
                    <OBLine backgroundColor="gray" height="0.1em" />
                    <Row>
                        <Col md="3" className="subTitle">
                            Project type
                    </Col>
                        <Col md="9">
                            <div onClick={this.showDropdownMenuProjectType}>
                                <input className="inputzone" placeholder="Select..." type="text" value={this.state.project_type} />
                                <img className="arrow" src={this.state.displayMenuProjectType ? require("../images/up-arrow.png") : require("../images/down-arrow.png")} />
                            </div>
                            {this.state.displayMenuProjectType ? (
                                <div className="menuList">
                                    <h1 className="h1-select" onClick={() => this.onClickHandlerProjectType("")}>Select...</h1>
                                    {
                                        datas_project_type.map((item) =>
                                            (this.state.project_type === item) ?
                                                <li style={{ listStyleType: "disc", color: "#fb17ce" }}><span>{item}</span></li>
                                                : <li onClick={() => this.onClickHandlerProjectType(item)}><span>{item}</span></li>
                                        )
                                    }
                                </div>
                            ) :
                                (
                                    null
                                )
                            }
                        </Col>
                    </Row>

                    <Row>
                        <Col md="3" className="subTitle" >
                            Days for the project
                    </Col>
                        <Col md="9">
                            <div>
                                <input
                                    type="range"
                                    min="0"
                                    max="100"
                                    step="1"
                                    value={this.state.x_day}
                                    onChange={this.handleChangeDay}
                                />


                                <h1 className="h1-range">{this.state.x_day}</h1>
                            </div>
                        </Col>
                    </Row>

                    <Row>
                        <Col md="3" className="subTitle">
                            Team members
                    </Col>
                        <Col md="9">
                            <div>
                                <input
                                    type="range"
                                    min="0"
                                    max="100"
                                    step="1"
                                    value={this.state.x_team}
                                    onChange={this.handleChangeTeam}
                                />
                                <h1 className="h1-range">{this.state.x_team}</h1>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="h1-title">
                            Project details
                    </Col>
                    </Row>

                    <OBLine backgroundColor="gray" height="0.1em" />
                    <Row>
                        <Col md="3" className="subTitle">Project speed</Col>
                        <Col md="9">
                            <div onClick={this.showDropdownMenuProjectSpeed}>
                                <input className="inputzone" placeholder="Select..." type="text" value={this.state.project_speed} />
                                <img className="arrow" src={this.state.displayMenuProjectSpeed ? require("../images/up-arrow.png") : require("../images/down-arrow.png")} />
                            </div>
                            {this.state.displayMenuProjectSpeed ? (
                                <div className="menuList">
                                    <h1 className="h1-select" onClick={() => this.onClickHandlerProjectSpeed("")}>Select...</h1>
                                    {
                                        datas_project_speed.map((item) =>
                                            (this.state.project_speed === item)
                                                ?
                                                <li style={{ listStyleType: "disc", color: "#fb17ce" }}><span>{item}</span></li>

                                                : <li onClick={() => this.onClickHandlerProjectSpeed(item)}><span>{item}</span></li>
                                        )
                                    }
                                </div>
                            ) :
                                (
                                    null
                                )
                            }
                        </Col>
                    </Row>

                    <Row>
                        <Col md="3" className="subTitle">Project precision</Col>
                        <Col md="9">
                            <div onClick={this.showDropdownMenuProjectPrecision}>
                                <input className="inputzone" placeholder="Select..." type="text" value={this.state.project_precision} />
                                <img className="arrow" src={this.state.displayMenuProjectPrecision ? require("../images/up-arrow.png") : require("../images/down-arrow.png")} />
                            </div>
                            {this.state.displayMenuProjectPrecision ? (
                                <div className="menuList">
                                    <h1 className="h1-select" onClick={() => this.onClickHandlerProjectPrecision("")}>Select...</h1>
                                    {
                                        datas_project_precision.map((item) =>
                                            (this.state.project_precision === item)
                                                ?
                                                <li style={{ listStyleType: "disc", color: "#fb17ce" }}><span>{item}</span></li>
                                                :
                                                <li onClick={() => this.onClickHandlerProjectPrecision(item)}><span>{item}</span></li>

                                        )
                                    }
                                </div>
                            ) :
                                (
                                    null
                                )
                            }
                        </Col>
                    </Row>

                    <Row>
                        <Col md="3" className="subTitle">Team dedication</Col>
                        <Col md="9">
                            <div onClick={this.showDropdownMenuTeamDedicated}>
                                <input className="inputzone" placeholder="Select..." type="text" value={this.state.team_dedicated} />
                                <img className="arrow" src={this.state.displayMenuTeamDedicated ? require("../images/up-arrow.png") : require("../images/down-arrow.png")} />
                            </div>
                            {this.state.displayMenuTeamDedicated ? (
                                <div className="menuList">
                                    <h1 className="h1-select" onClick={() => this.onClickHandlerTeamDedicated("")}>Select...</h1>
                                    {
                                        datas_team_dedication.map((item) =>
                                            (this.state.team_dedicated === item) ?
                                                <li style={{ listStyleType: "disc", color: "#fb17ce" }}><span>{item}</span></li>
                                                :
                                                <li onClick={() => this.onClickHandlerTeamDedicated(item)}><span>{item}</span></li>
                                        )
                                    }
                                </div>
                            ) :
                                (
                                    null
                                )
                            }
                        </Col>
                    </Row>

                    <Row>
                        <Col md="3" className="subTitle">Include promo material</Col>
                        <Col md="5">
                            <ToggleSwitch />
                        </Col>
                    </Row>


                    {
                        this.state.showNext ?
                            <Row>
                                <Col md="12">
                                    <button className='bottomTotal'>
                                        <span className="textTotal"> Total</span>
                                    </button>
                                    <button className='bottomTotalNumber'>
                                        <span className="textTotal"> $0.00</span>
                                    </button>
                                </Col>
                            </Row>
                            :
                            <Row>
                                <Col md="9">
                                    <button className='bottomTotal'>
                                        <span className="textTotal"> Total</span>
                                    </button>
                                    <button className='bottomTotalNumber'>
                                        <span className="textTotal"> $0.00</span>
                                    </button>
                                </Col>
                                <Col md="3">
                                    <button className='bottomNext' type="submit" onClick={this.onClickNext}>
                                        <span className="textNext"> Next</span>
                                        <img className='iconNext' src={require("../images/next.png")} />
                                    </button>
                                </Col>
                            </Row>
                    }
                    {
                        this.state.showNext ?
                            this._renderNext() : null
                    }

                </Container>
            </div>
        );
    }
}
export default RigthBloc;