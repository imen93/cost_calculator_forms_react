import React from 'react';
import RigthBloc from './screens/RigthBloc';
import LeftBloc from './screens/LeftBloc'

function App() {
  return (
    <div style={{ display: "flex" }}>
      <div style={{ width: "15vw", height: "100vh", backgroundColor: "white" }}></div>
      <LeftBloc />
      <RigthBloc />
    </div>
  );
}

export default App;