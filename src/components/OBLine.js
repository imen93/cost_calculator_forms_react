import React, { Component } from 'react';
class OBLine extends Component {
    render() {
        return (
            <hr
                style={{
                    backgroundColor: this.props.color,
                    height: this.props.height
                }}
            />
        );
    }
}
export default OBLine;