import React, { Component } from 'react';
import '../css/InputText.css'
class OBInputText extends Component {
    render() {
        return (
            <div className="field">
                <input
                    className="inputText"
                    type={this.props.type}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    onChange={this.props.handleChange}
                    style={this.props.style}
                />
            </div>
        );
    }
}
export default OBInputText;