import React, { Component } from 'react';
import { Container, Col, Row } from 'reactstrap';
import '../css/Card.css'
class OBCard extends Component {
    render() {
        return (
            <Container>

                <Row>
                    <Col md="3">
                        <img className="icon" src={require("../images/user.png")} />
                    </Col>
                    <Col>
                        <h1 className="title">{this.props.title}</h1>
                        <p className="descriptionTitle">{this.props.description}</p>
                    </Col>
                </Row>
            </Container>
        );
    }
}
export default OBCard;